# LSB Steganography
[![Go Reference](https://pkg.go.dev/badge/gitlab.com/chu4ng/lsb.svg)](https://pkg.go.dev/gitlab.com/chu4ng/lsb)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/chu4ng/lsb)](https://goreportcard.com/report/gitlab.com/chu4ng/lsb)

Steganography is the practice of representing information within another message or physical object, in such a manner that the presence of the information is not evident to human inspection.
LSB(Least Significant Bit)

This is LSB-stegonagraphy algorithm implementation, which uses an image(carrier) to hide secret message.

## Install
```bash
go install gitlab.com/chu4ng/lsb@latest
```
or from source code:
```bash
git clone https://gitlab.com/chu4ng/lsb && cd lsb
go build
```

## Usage
Decoding:
```bash
lsb -carrier name.png
```
Encoding:
```bash
lsb -carrier name.png -output secret_name.png -data "your message here"
# or
lsb -carrier name.png -prefix "secret_" -data "your message here"
```
## Example
Use file as data via bash functional:
```bash
cat file | lsb -c img.png -p s_ -d -
# or
lsb -c img.png -p s_ -d - <file
```
Use user input.
```bash
lsb -c img.png -p s_ -d -
# Type `ctrl+d` to interrupt typing and start execution
```

## Flags
```
  -c string
  -carrier string
    	which file to hide data into
  -d string
  -data string
    	what data to hide. When string is '-', read standart input
  -o string
  -output string
    	filename for image which carrie hidded data
  -p string
  -prefix string
    	prefix which added to original filename and used as output filename. It's adding without delimeter, keep it in mind
```

## Notes
In plans to add:
- [x] Reading from stdio, so you can pipe data into image
- [x] Add gzip compression
- [x] encryption with password
- [ ] if message is bigger than image capacity, try to use last N bits instead of just last one.
- [x] BUG: without hidden content, it still will try to find message_size and iterate over image, which could cause stepping out of boundary.
- [x] make a flag somewhere in message which will indicate if message is even present or it's just an image
- [x] BUG: there's visible changes on an image
- [ ] add goroutines which handles pixels?
- [ ] make an interface from "message" BS.

## How it works
We use RGB pixel and encode a few bits into them.
For example, we have a character 'A', decimal representation of whom is 65, binary 01000001(leading zero, because we use all 8 bits).
For color, lets pick #7955c0, RGB(121,85,192), in binary 1111001 1010101 11000000.
As elaboration to LSB says, we use last bits of number and change them. For our or any character we need a few pixels though, if we use one last bit. Because of that, let's encode a smaller number 5(0b101).
Red color 1111001 doesn't change, but blue will become 1010100, 84 instead of 85. Same goes for green: 110000001(193).
Change is so insignificant that human eye can't tell the difference.
