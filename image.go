package main

import (
	"image"
	"image/png"
	"os"
)

func OpenImage(filename string) (image.Image, error) {
	reader, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	img, _ := png.Decode(reader)

	if err != nil {
		return nil, err
	}

	return img, nil
}

func SaveImage(filename string, img image.Image) error {
	fd, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return err
	}
	err = png.Encode(fd, img)
	if err != nil {
		return err
	}
	return nil
}
