/* lsb allow you encode secret message inside png-image container.
It uses Least Significant Bit algorithm.

Usage:
# Encode
    lsb -carrier filename  (-output outfilename | -pre prefix) -data "your message"
# Decode
    lsb -carrier filename
*/

package main

import (
	"flag"
	"fmt"
	"gitlab.com/chu4ng/lsb/stego"
	"io"
	"log"
	"os"
	"path"
)

var carrierFilename *string
var outputFilename *string
var prefixOutFilename *string
var dataToHide *string
var passPhrase *string

var flagAlias = map[string]string{
	"carrier": "c",
	"output":  "o",
	"prefix":  "p",
	"data":    "d",
	"pass":    "P",
}

func init() {
	carrierFilename = flag.String("carrier", "", "which file to hide data into")
	outputFilename = flag.String("output", "", "filename for image which carrie hidded data")
	prefixOutFilename = flag.String("prefix", "", "prefix which added to original filename and used as output filename. It's adding without delimeter, keep it in mind'")
	dataToHide = flag.String("data", "", "what data to hide. When string is '-', read standart input")
	passPhrase = flag.String("pass", "", "encode your message with given string")
	for from, to := range flagAlias {
		flagSet := flag.Lookup(from)
		flag.Var(flagSet.Value, to, fmt.Sprintf("alias to %s", flagSet.Name))
	}

	flag.Usage = func() {
		fmt.Println("USAGE:")
		fmt.Println("Decoding")
		fmt.Printf("\tlsb -c filename.png\n")
		fmt.Println("Encoding")
		fmt.Printf("\tlsb -c filename.png (-p prefix or -o outfilename) -d \"data to hide\"\n")
		fmt.Println("NOTE: if data is -, read data from input stream. Don't use '-data' for files")
		flag.PrintDefaults()
	}
}

func checkFlags() {
	if len(*carrierFilename) == 0 {
		fmt.Println("Error: Carrier file name should be specified. Use lsb --help for more information.")
		fmt.Println()
		flag.Usage()
		os.Exit(1)
	}
}

func main() {
	flag.Parse()
	checkFlags()

	carrier, err := OpenImage(*carrierFilename)
	if err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}

	//Decoding
	if *dataToHide == "" { // No data specified, so we decode given image

		response, err := stego.DecodeImage(carrier)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		err = response.UnGzipData()

		if err != nil {
			fmt.Printf("can't unzip: %s\n", err)
			os.Exit(1)
		}
		if err != nil {
			fmt.Printf("No hidden data inside %s\n", *carrierFilename)
			os.Exit(1)
		}

		if len(*passPhrase) != 0 {
			response.XORBits(*passPhrase)
		}

		fmt.Fprintf(os.Stderr, "Response: ")
		fmt.Fprintf(os.Stdout, "%s", response.String())
		os.Exit(0)
	}

	bytesToHide := []byte(*dataToHide)

	if *dataToHide == "-" {
		bytesToHide, err = io.ReadAll(os.Stdin)
		if err != nil {
			log.Fatal(err)
		}
	} else if *dataToHide == "" {
		fmt.Println("ERROR: No data provided, see --usage for information")
		os.Exit(0)
	}
    msgToHide :=  stego.CreateMockMessage(bytesToHide)

	// Encoding
	if len(*passPhrase) != 0 {
		msgToHide.XORBits(*passPhrase)
	}
	err = msgToHide.GzipData()
	if err != nil {
		log.Fatal("zipData", err, "\n")
	}

	message := stego.CreateMessage(msgToHide.Bits())

	newImg, err := stego.EncodeImage(carrier, message)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if len(*prefixOutFilename) == 0 && len(*outputFilename) == 0 {
		fmt.Println("Error: Either output filename or prefix should be specified")
		fmt.Println()
		flag.Usage()
		os.Exit(1)
	}
	if *outputFilename == "" {
		dirpath := path.Dir(*carrierFilename)
		*outputFilename = path.Join(dirpath, *prefixOutFilename+path.Base(*carrierFilename))
	}

	err = SaveImage(*outputFilename, newImg)
	if err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}

	fmt.Printf("%s: Success\n", *outputFilename)
}
