build:
    go build

run: build
    ./lsb -c images/test.png -p s_ -d - <README.md
    #./lsb -c images/test.png -p s_ -d "Secret message"
    ./lsb -c images/s_test.png | head

time: build
    time ./lsb -c images/big.png -p s_ -d - <big

test: build
    go test ./...

fmt:
    go fmt ./...
