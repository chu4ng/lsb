package stego

import (
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"math"
)

var (
	SIZEFIELD_SIZE = 4
)

type Message struct {
	bits []byte
}

// ReadByte simply return byte at the beggining of underlying bits array, until nothing left.
func (b *Message) ReadByte() (byte, error) {
	if len(b.bits) == 0 {
		return 0, errors.New("BitQuee: EOF")
	}
	ret := b.bits[0]
	b.bits = b.bits[1:]
	return ret, nil
}

func (b *Message) String() string {
	return string(b.bits)
}

func (b *Message) Bits() []byte {
	return b.bits
}

// XORBits make a sha256 hash.Hash from passphrase arg,
// then iterate over secret message and do XOR operation over each byte.
func (m *Message) XORBits(passphrase string) {
	// Transformate to sha256, so minor changes in passphrase will matter!
	passphrase = func(s string) string {
		h := sha256.New()
		h.Write([]byte(passphrase))
		return string(h.Sum(nil))
	}(passphrase)

	xorBuf := []byte{}
	codeIndex := 0
	for _, b := range m.bits {
		xorBuf = append(xorBuf, b^passphrase[codeIndex%len(passphrase)])
	}
    m.bits = xorBuf
}


func CreateMockMessage(data []byte) *Message {
	return &Message{data}
}

// CreateMessage gets the secret message and return kinda iterator object.
// Firstly it calculate message size, put it at the beggining of the buffer and convert all in binary representation.
func CreateMessage(data []byte) *Message {
	// Encode length into first 4 bytes
	sizeBuf := calculateMsgSize(len(data))

	encBuf := EncodeToBits(sizeBuf, data)

	return &Message{encBuf}
}

func EncodeToBits(bitsToEncode ...[]byte) []byte {
	var bitsBuf []byte
	for _, ba := range bitsToEncode {
		for _, b := range ba {
			bitsBuf = append(bitsBuf, decToBin(b)...)
		}
	}
	return bitsBuf
}

func calculateMsgSize(l int) []byte {
	sizeBuf := make([]byte, 4)

	// Encode length into first 4 bytes
	binary.LittleEndian.PutUint32(sizeBuf, uint32(l))
	return sizeBuf
}

// decToBin return array of bytes as bitstream representation.
// Using reverse order (BigEndian)
func decToBin(n byte) []byte {
	buf := []byte{}
	for i := 0; i < 8; i++ {
		buf = append(buf, byte(n%2))
		n >>= 1
	}
	return buf
}

func binToDec(arr []byte) byte {
	var sum byte
	for i, bit := range arr {
		sum += (byte(math.Pow(2, float64(i)))) * bit
	}
	return sum
}

// DecodeMessage read bytes and return hidden message.
// At the start it reads first 32 bits as the size of the hidden message.
// Then it reads specified bytes and convert it into decimal and return array of bytes.
func DecodeMessage(dataBuffer []byte) []byte {

	sizeBuf := DecodeBitsChank(SIZEFIELD_SIZE, dataBuffer)
	messageSize := binary.LittleEndian.Uint32(sizeBuf)

	charBuffer := DecodeBitsChank(int(messageSize), dataBuffer[32:])
	return charBuffer

}

// DecodeMsgSize gets bytes array with, read first 3 bytes and return size of hidden message.
// LSB try to decode hidden msg even if it's not present whoch leads to fatal error.
// Now we check if decoded "size"(might be false) is in boundaries of pixel matrix.
// It's not perfect, but before adding metadata and flags to message, that works without crash.
func DecodeMsgSize(dataBuffer []byte) uint32 {
	sizeBuf := DecodeBitsChank(SIZEFIELD_SIZE, dataBuffer)
	messageSize := binary.LittleEndian.Uint32(sizeBuf)
	return messageSize
}

func DecodeBitsChank(lengthInBytes int, bitsSlice []byte) []byte {
	buffer := []byte{}
	for i := 0; i < lengthInBytes*8; i += 8 {
		buffer = append(buffer, binToDec(bitsSlice[i:i+8]))
	}
	return buffer
}
