package stego

import (
	"bytes"
	"errors"
	"image"
	"image/color"
)

var (
	hiddenFlagSize = 3 // I want to encode 170(0b10101010) three times, so I need 8*3 pixels
)

// EncodeImage iterate over all pixels and encode secret msg if it present.
func EncodeImage(m image.Image, msg *Message) (image.Image, error) {
	b := m.Bounds()
	newImg := image.NewRGBA(b)
	var newColor color.Color
    var pickMsgToHide *Message

	// TODO In future I want to be able encode N bits, instead of just last one.
	if b.Dx()*b.Dy() <= len(msg.bits)+(hiddenFlagSize) {
		return nil, errors.New("container is too small for your message")
	}

	hiddenFlagMsg := &Message{bits: EncodeToBits(hiddenFlag())}

	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			if y == b.Max.Y-1 && b.Max.X-8 <= x {
                pickMsgToHide = hiddenFlagMsg
			} else {
                pickMsgToHide = msg
			}
            newColor = encodePixel(m.At(x, y), pickMsgToHide)
			newImg.Set(x, y, newColor)

		}
	}
	return newImg, nil
}

func hiddenFlag() []byte {

	flagBuf := []byte{}
	for i := 0; i < hiddenFlagSize; i++ {
		flagBuf = append(flagBuf, 170)
	}

	return flagBuf
}

// encodePixel disassemble each given pixel into RGBA representation, though Alpha-channel is dropped, then encode 3 bits of secret msg
// It returns pixel with encoded 3 bits of msg.
func encodePixel(pixel color.Color, msg *Message) color.Color {
	r, g, b, a := pixel.RGBA()
	r = encodeByte(r, msg)
	g = encodeByte(g, msg)
	b = encodeByte(b, msg)
	return color.RGBA{uint8(r), uint8(g), uint8(b), uint8(a)}
}

// encodeByte encode one bit of encoded message to one bit of given color "bit"
// NOTE color.Color actually uses uint32
func encodeByte(c uint32, msg *Message) uint32 {

	bitToHide, err := msg.ReadByte()
	if err != nil {
		return c
	}

	// Color is encoded to uint32 by default, but we need byte/uint8
	c &= c >> 8

	var mask uint32 = 0xfffffffe
	encodedBit := (c & mask) | uint32(bitToHide)
	encodedBit |= encodedBit << 8

	return encodedBit
}

// DecodeImage iterate over all pixels of image and return secret msg
func DecodeImage(m image.Image) (*Message, error) {
	hiddenBuffer := []byte{}

	if !flagIsSet(m) {
		return &Message{}, errors.New("image doesn't have hidden message inside")
	}

	b := m.Bounds()
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			decoded := decodePixel(m.At(x, y))
			hiddenBuffer = append(hiddenBuffer, decoded...)
		}
	}

	msgSize := DecodeMsgSize(hiddenBuffer[:32])
	msg := Message{DecodeBitsChank(int(msgSize), hiddenBuffer[32:])}

	return &msg, nil

}

func flagIsSet(img image.Image) bool {
	b := img.Bounds()

	flagBuf := []byte{}
	for x := b.Max.X - 8; x < b.Max.X; x++ {
		flagBuf = append(flagBuf, decodePixel(img.At(x, b.Max.Y-1))...)
	}

	return bytes.Equal(flagBuf, EncodeToBits(hiddenFlag()))
}

// decodePixel return LSB of pixel for each color
func decodePixel(c color.Color) []byte {
	r, g, b, _ := c.RGBA()
	r2 := decodeByte(r)
	g2 := decodeByte(g)
	b2 := decodeByte(b)
	// It's not a color.Color, but a sequence of bytes in the message
	return []byte{r2, g2, b2}
}

// decodeByte return one last bit of number
func decodeByte(c uint32) byte {
	c &= c >> 8
	return byte(c & 0b1)
}
