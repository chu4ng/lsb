package stego

import (
	"bytes"
	"compress/gzip"
	"io"
)

func (m *Message) UnGzipData() (err error) {
	b := bytes.NewBuffer(m.bits)

	var r io.Reader
	r, err = gzip.NewReader(b)
	if err != nil {
		return
	}

	var resB bytes.Buffer
	_, err = resB.ReadFrom(r)
	if err != nil {
		return
	}

	m.bits = resB.Bytes()

	return
}

func (m *Message) GzipData() (err error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)

	_, err = gz.Write(m.bits)
	if err != nil {
		return
	}

	if err = gz.Flush(); err != nil {
		return
	}

	if err = gz.Close(); err != nil {
		return
	}

	m.bits = b.Bytes()

	return
}
